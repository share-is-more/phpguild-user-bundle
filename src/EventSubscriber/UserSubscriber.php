<?php

declare(strict_types=1);

namespace PhpGuild\UserBundle\EventSubscriber;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use PhpGuild\UserBundle\Model\UserInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class UserSubscriber
 */
#[AsDoctrineListener(event: Events::prePersist, connection: 'default')]
#[AsDoctrineListener(event: Events::preUpdate, connection: 'default')]
final class UserSubscriber
{
    /** @var UserPasswordHasherInterface $encoder */
    private $encoder;

    /**
     * UserSubscriber constructor.
     *
     * @param UserPasswordHasherInterface $encoder
     */
    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function prePersist(PrePersistEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof UserInterface) {
            return;
        }

        $this->resolveObject($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof UserInterface) {
            return;
        }

        $this->resolveObject($entity);
    }

    /**
     * resolveObject
     *
     * @param UserInterface $entity
     *
     * @throws \Exception
     */
    public function resolveObject(UserInterface $entity): void
    {
        if (!$entity->getSalt()) {
            $entity->setSalt(hash_hmac('sha256', random_bytes(128), random_bytes(128)));
        }

        if (!$entity->getUsername()) {
            $entity->setUsername($entity->getEmail());
        }

        if ($entity->getPlainPassword()) {
            $entity->setPassword($this->encoder->hashPassword($entity, $entity->getPlainPassword()));
            $entity->eraseCredentials();
        }

        if (!$entity->getPassword()) {
            $entity->setPassword(hash_hmac('sha256', random_bytes(128), random_bytes(128)));
        }
    }
}
